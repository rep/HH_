#!/usr/bin/python
# coding: utf-8


import Image
import sys
import ImageDraw
from PIL.Image import composite,LINEAR

# MILLISECONDES
from datetime import datetime
dt = datetime.now()
dt.microsecond

im1 = Image.open(str(sys.argv[1])).convert('RGBA')
im2 = Image.open(str(sys.argv[2])).convert('RGBA')
im3 = Image.new("1", (512,512), "white")

draw = ImageDraw.Draw(im3)
draw.ellipse(((100,100),(200,200)), fill="black")
draw.ellipse(((180,180),(300,300)), fill="black")

scriptpy = str(sys.argv[1])
script = scriptpy[:-4]
heure = str(dt.hour)+"h"+str(dt.minute)+"m"+str(dt.second)+"s"+str(dt.microsecond)+"ms"

im1.paste(im2, im3)

im1.save(script+".1.alpha."+heure+".png",'PNG', quality=100)
im2.save(script+".2.alpha."+heure+".png",'PNG', quality=100)
im3.save(script+".3.alpha."+heure+".png",'PNG', quality=100)
