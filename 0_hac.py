#!/usr/bin/python
# coding: utf-8

import sys
import Image
import random
import os
import ImageDraw
import ImageFont
import ImageFilter
import time
import ImageEnhance
import pickle

# MILLISECONDES
from datetime import datetime
dt = datetime.now()
dt.microsecond
heure = str(dt.hour)+"h"+str(dt.minute)+"m"+str(dt.second)+"s"+str(dt.microsecond)+"ms"

print dt

# AGRANDISSEMENT
allongement = 3

# IMAGES
im1 = Image.open(str(sys.argv[1])).convert('RGBA')
im2 = Image.new("RGBA",(im1.size[0], im1.size[1]))
im3 = Image.new("RGBA",(im1.size[0], im1.size[1]))
im4 = Image.new("RGBA",(im1.size[0], im1.size[1]))
im5 = Image.new("RGBA",(im1.size[0], im1.size[1]))
im6 = Image.new("RGBA",(im1.size[0]*allongement, im1.size[1]))
im7 = Image.new("RGBA",(im1.size[0], im1.size[1]), "white") # masque alpha

# LARGEUR HAUTEUR
Larg = im1.size[0]
Haut = im1.size[1]
loadfile = False

# EMPLACEMENT SCRIPT
scriptpy = str(sys.argv[1])
script = scriptpy[:-4]

def randHaut():
	return random.randint(0, im1.size[1]/16)*16

randomCoupeHauteur1 = [0, \
randHaut(),randHaut(),randHaut(),randHaut(), \
randHaut(),randHaut(),randHaut(),randHaut(), \
randHaut(),randHaut(),randHaut(),randHaut(), \
randHaut(),randHaut(),randHaut(),randHaut(), \
randHaut(),randHaut(),randHaut(),randHaut(), \
randHaut(),randHaut(),randHaut(),randHaut(), \
im1.size[1]]

print  randomCoupeHauteur1
# ENLEVE DOUBLONS
randomCoupeHauteur = set(randomCoupeHauteur1)
randomCoupeHauteur = list(randomCoupeHauteur)

print randomCoupeHauteur

# RANGEMENT
randomCoupeHauteur.sort()
print  randomCoupeHauteur
	
# HAACHEUUR
def Hacheur(haut, bas) :
	n = 0
	i = 0
	while n<im6.size[0] :
		i+=1
		loop = 0
		
		# PROPORTIONS
		proportions = [\
#		(2,2),(2,4),(2,5),(2,8),(2,16),(2,32),\
#		(4,4),(4,5),(4,8),(4,16),(4,32),(4,64),\
#		(8,3),(8,5),(8,8),(8,16),(8,32),\
		(16,2),(16,3),(16,4),(16,5),(16,8),(16,16),(16,32),\
		(32,3),(32,5),(32,8),(32,16),(32,32),\
		(128,1),(128,2),(128,4),(128,8),\
		(256,1),(256,2),(256,4),\
		(512,1),(512,2)]
		
		'''
		(768,1),(768,2),\
		(1024,1),(1024,2),\
		(2048,1),\
		(3072,1)]
		'''
		
		# COPY POS X 
		choix_rnd = random.randint(0, len(proportions)-1)
		largeur = proportions[choix_rnd][0]
		copyPosX = random.randint(0, (im1.size[0]-largeur))
		largeur = proportions[choix_rnd][0]			
		repeat = proportions[choix_rnd][1]
		
		# PIXEL SIZE
		pixelSizeList = [1]
		#pixelSizeList = [1, 1, 1, 1, 2, 2, 4, 8, 16, 32, 64, 128, 256]
		#pixelSizeList = [1,5,25,125]
		pixelSizeIndex = random.randint(0,len(pixelSizeList)-1)
		pixelSize = pixelSizeList[pixelSizeIndex]
		
		# CROP
		hauteur = bas-haut
		cropfinal = [largeur,hauteur]
				
		im2 = im1.crop((copyPosX,haut,copyPosX+largeur,haut+hauteur))
		im3 = im2.resize((im2.size[0]/pixelSize, im2.size[1]/pixelSize), Image.NEAREST)
		im4 = im3.resize((im3.size[0]*pixelSize, im3.size[1]*pixelSize), Image.NEAREST)
		im5 = im4.crop((0,0,cropfinal[0],cropfinal[1]))
		
		# MASQUE
		im7 = Image.new("1", (cropfinal[0],cropfinal[1]), "black")
		l = im7.size[0]
		h = im7.size[1]
		im7draw = ImageDraw.Draw(im7)	
		n1 = 0
		ecart = 3
		tranches = h / ecart
		while n1 < tranches :
			rx = random.randint(0,l/2)
			im7draw.rectangle(((rx,n1*tranches),(rx+l/2,n1*tranches+tranches)),"white")
			n1 = n1 + 1
		
		# REPETITION MOTIF MASQUÉ
		while loop<repeat:
			px1 = n
			px2 = n + cropfinal[0]
			im6.paste(im5, (px1, haut, px2, bas), im7)
			n = n + l/2			
			loop = loop + 1
		n = n - im7.size[0]/2	

for j in range(len(randomCoupeHauteur)-1):
	Hacheur(randomCoupeHauteur[j], randomCoupeHauteur[j+1])

#TODO : inclure version du script + taille finale image
#im6.putalpha(255)
im6.save(script+"."+heure+".png",'PNG', quality=100)
im6.show()
