#!/usr/bin/python
# coding: utf-8

# haacheuur 0.24
# port industriel de port la nouvelle - couleur - 60cm*30cm
# image source : pln.jpg
# image obtenue : pln..20150910-11h59m53s.jpg

import sys
import Image
import random
import os
import ImageDraw
import ImageFont
import ImageFilter
from time import gmtime, strftime
import time

# modifs du 30/10/2013
import ImageEnhance

'''
multiples 1024	: 	1024 2048 3072 4096 5120 		6144 7168 8192 		9216 10240
multiples 768 :	768 1536 2304 3072 3840 4608 5376 	6144 6912 7680 8448 9216 9984 10752 

'''


#rapport d'allongement de la nouvelle image par rapport à la largeur de l'image originale
allongement = 1

#ouvertrure de l'image source et conversion en mode couleur 1bit
#im1 = Image.open(str(sys.argv[1])).convert('1')
#im1 = Image.open(str(sys.argv[1])).convert('L')
im1 = Image.open(str(sys.argv[1]))
im2 = im1.copy()

im4 = Image.new("RGBA",(im1.size[0]*allongement, im1.size[1]))
im5 = Image.new("RGBA",(im1.size[0]*allongement, im1.size[1]))

boutImage = Image.new("RGBA",(im1.size[0], im1.size[1]))

Larg = im1.size[0]
Haut = im1.size[1]
import pickle

loadfile = False

class Sequence:
	def __init__(s):
		randomCoupeHauteur = []
		s.randomCopyPosi =[]
		s.proportions=[]
		s.choix=[]
		s.sizeOutput=None
		s.sizeInput=None

"""
seq = dict()
seq["randomCoupeHauteur"] = []
seq["randomCopyPosi"] = [] 
seq["proportions"] = []
seq["choix"] = []
seq["sizeOutput"]= im4.size
seq["sizeInput"] = im1.size
"""
if loadfile:
	seq=pickle.load(open("test.pickle"))

else :
	seq=Sequence()
	
'''
for i in range(1):
'''
	
		
# constitution de la liste des tranches horizontales
# genre comme si qu'on avait un 16 pistes :)
# nombre aleatoire compris dans les limites de l'image
def randHaut():
	return random.randint(0, im1.size[1]/8)*8

if loadfile:
	randomCoupeHauteur = seq.randomCoupeHauteur
	
else:
	randomCoupeHauteur = [0, \
	randHaut(),randHaut(),randHaut(),randHaut(), \
	randHaut(),randHaut(),randHaut(),randHaut(), \
	randHaut(),randHaut(),randHaut(),randHaut(), \
	randHaut(),randHaut(),randHaut(),randHaut(), \
	randHaut(),randHaut(),randHaut(),randHaut(), \
	randHaut(),randHaut(),randHaut(),randHaut(), \
	randHaut(),randHaut(),randHaut(),randHaut(), \
	randHaut(),randHaut(),randHaut(),randHaut(), \
	im1.size[1]]
	# rangement des valeurs des plus petites au plus grandes
	randomCoupeHauteur.sort()
	seq.randomCoupeHauteur = randomCoupeHauteur
	
# les hachures
def Hacheur(haut, bas) :
	
	n=0
	i=0
	#!!!!!!!!!!
	while n<im4.size[0] :
		
		if loadfile:
			proportions = seq.proportions
			choix_rnd = seq.choix[i]
			randomCopyPosi = seq.randomCopyPosi[i]
		else:
			
			
			
			
			
			
			proportions = [\
			(2,8),(2,16),(2,32),\
			(4,4),(4,8),(4,16),(4,32),(4,64),\
			(8,8),(8,16),(8,32),\
			(16,2),(16,4),(16,8),(16,16),(16,32),\
			(32,8),(32,16),(32,32),\
			(64,4),(64,8),(64,16)]
			'''			
			(128,2),(128,4),(128,8),\
			(256,1),(256,2),(256,4),\
			(512,1),(512,2),\
			(1024,1),(1024,2),\
			(2048,1)]
			'''
			
			seq.proportions = proportions
			#proportions = seq.proportions[]
			choix_rnd = random.randint(0, len(proportions)-1)
			#choix_rnd = random.randint(0, len(proportions)-1)
			seq.choix.append(choix_rnd)
			
			largeur = proportions[choix_rnd][0]

			# positionnement de la copie, aleatoirement, entre 0 et la largeur totale de l'image 
			randomCopyPosi = random.randint(0, (im1.size[0]-largeur-32))
			#randomCopyPosi = seq.randomCopyPosi
			seq.randomCopyPosi.append(randomCopyPosi)
		
		i+=1

		# tirage au sort
		
		#seq["choix"].append(choix_rnd)
		
		# assignation des valeurs (paires) finales choisies
		largeur = proportions[choix_rnd][0]			
		repeat = proportions[choix_rnd][1]

		#seq["randomCopyPosi"].append(randomCopyPosi)
		
		cx1 = randomCopyPosi
		cx2 = randomCopyPosi + largeur
		#print "im3 = im2.crop : "+str(im3.size)
			
		#draw = ImageDraw.Draw(im4)
				
		loop = 0
		
		#pixelSizeList = [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,2,2,4,4,8]
		pixelSizeList = [1,8,16,32]
		pixelSizeIndex = random.randint(0,len(pixelSizeList)-1)
		pixelSize = pixelSizeList[pixelSizeIndex]
		
		#print " pixelSize = "+str(pixelSize)
		
		
		cropfinal = [largeur,bas-haut]
		hauteur = bas-haut
		
		'''
		haut gauche(0,0).
		haut droit (largeur-1 , 0).
		bas gauche (0 , hauteur-1).
		bas droit (largeur-1, hauteur-1)
		'''
		
		print
		print "-------------------------------------------"
		print "largeur="+str(largeur)
		print "hauteur="+str(hauteur)
		print pixelSize
		
		if largeur % pixelSize != 0:
			croop = int(largeur / pixelSize)
			largeur = (croop + 1 ) * pixelSize
			print "croop="+str(croop)
			print "pixelSize="+str(pixelSize)
			print "largeur="+str(largeur)
			
		if  hauteur % pixelSize != 0:
			croop2 = int(hauteur / pixelSize)
			hauteur = (croop2 + 1 ) * pixelSize
			print "hauteur="+str(hauteur)
		
		
		# decoupage du sample :
		# sample aux dimensions de l'effet
		im5 = im2.crop((randomCopyPosi,haut,randomCopyPosi+largeur,haut+hauteur))
		
		
		''' PIXELISATION '''
		if pixelSize > 1 :
			boutImage = im5.resize((im5.size[0]/pixelSize, im5.size[1]/pixelSize), Image.NEAREST)
			boutImage = im5.resize((im5.size[0]*pixelSize, im5.size[1]*pixelSize), Image.NEAREST)
		else :
			boutImage = im5
		
		# le sample final (bonnes dimensions)
		im3 = boutImage.crop((0,0,cropfinal[0],cropfinal[1]))
		
		print "im5.size="+str(im5.size)
		print "im3.size1="+str(im3.size)
		
		''' COLLAGE DU SAMPLE
		'''

		while loop<repeat:
			px1 = n
			px2 = n + cropfinal[0]
			#lignes colorées de 1 pix
			#draw = ImageDraw.Draw(boutImage)
			#draw.line((0,0,0,4), fill="rgb(128,128,128)")
			
			draw = ImageDraw.Draw(im3)
			draw.line((0, 0, largeur-1, 0), fill="rgb(255,255,255)")
			draw.line((largeur-1, 0, largeur-1, bas-haut), fill="rgb(255,255,255)")
			
			print "px2-px1="+str(px2-px1)
			print "bas-haut="+str(bas-haut)
			print "im4.size="+str(im4.size)
			print
			
			im4.paste(im3, (px1, haut, px2, bas))
			
			n = n + largeur
			loop = loop + 1
			
		print
''' OPTIONS '''	
def TrancheHorizontale() :
	# tirage au hasard de la bande copiee
	pos = random.randint(0, im1.size[1]-im1.size[1]/20)
	# copiage
	im5 = im2.crop((0,pos,im1.size[0],pos+im1.size[1]/20))
	
	# le soulignage en blanc
	draw = ImageDraw.Draw(im5)
	draw.line((0, im5.size[1]-1, im5.size[0], im5.size[1]-1), fill="black")
	draw.line((0, 1, im5.size[0], 1), fill="black")
	
	# collage	
	im4.paste(im5, (0,pos,im1.size[0],pos+im1.size[1]/20))

# HAACHEUUR
for j in range(len(randomCoupeHauteur)-1):
	Hacheur(randomCoupeHauteur[j], randomCoupeHauteur[j+1])

''' SAUVEGARDE '''
# CTRL + S
#chemin du script	: scriptpy = sys.argv[0]
#chemin de l'image	: str(sys.argv[1])
scriptpy = str(sys.argv[1])
script = scriptpy[:-3]
#n = "%05d" % i
n = "1.1"
im4.save(script+"."+n+"_"+strftime("%Y%m%d-%Hh%Mm%Ss", gmtime())+".png",'PNG', quality=100)
pickle.dump(seq, open(script+"."+n+"_"+strftime("%Y%m%d-%Hh%Mm%Ss", gmtime())+".pickle","w"))

#print script+"."+str(i)+"_"+strftime("%Y%m%d-%Hh%Mm%Ss", gmtime())+".jpg"
#millis = int(round(time.time() * 1000))
#print "millis-secondes : "+str(millis)

