#!/usr/bin/python
import os, sys, Image, ImageDraw, ImageColor
from time import gmtime, strftime

fichier=sys.argv[1]

try:
	im1 = Image.open(fichier).convert('1')
	im2 = im1.copy()
	im3 = im1.copy()
	draw = ImageDraw.Draw(im2)
except IOError:
	print "FICHIER!!!", infile

date = strftime("%Y%m%d-%Hh%Mm%Ss", gmtime())

x=0
while x < im1.size[0] :
	y = 0
	while y < im1.size[1] :
		colorpixel = im1.getpixel((x,y))
		draw.line( (0, y, im2.size[0], y) , fill=colorpixel)
		draw.line((x, 0, x, im2.size[1]-1), fill=1)
		
		y = y + 1
		
	# ALPHA tout bete :
	im3 = Image.blend(im1, im2, 0.8)
	
	n = "%05d" % x
	nomdefichier = "horizontal"+n+".png"
	im3.save(nomdefichier, "PNG")
	print nomdefichier
	
	x = x + 1
	
del draw

os.system("ffmpeg -i horizontal%05d.png -i "+fichier+".enscribe.norm.wav -c:a libfaac -c:v libx264 -preset ultrafast -crf 32 "+fichier+".final.mp4")
print " OUTPUT FINAL : "+fichier+".final.mp4"

# Nettoyage
os.system("rm horizontal*.png")
