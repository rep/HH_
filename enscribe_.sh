# ------------- FILE INFO OUTPUT

file = $1
echo 'file : '+file

# ------------- RESIZE IMAGE (512, 1024, 2048, 4096 pixels de haut)
# donc de 5184*3456 pix à 1536*1024 pix par exemple
#convert $image_input -resize 768x512 $image_output_resize

# ------------- HACHAGE
#python ~/Projets/haacheur/hacheuur.py $image_output_resize

# ------------- HORIZONTAL


# ------------- ROTATE IMAGE 90 DEGRES SENS HORAIRE
#convert -rotate 90 $image_input $image_output


# ------------- ENSCRIBE 
# 1536 pixels / 25 fps = 61 secondes (durée fichier audio)
# enscribe -ts 1024 -hf 20 -o output.wav input.wav -mask /usr/local/share/horizontal/sounds/piano_7.flac -lenght $tailleImage/$fps
# enscribe -ts=1 -hf=20 -mask /usr/local/share/horizontal/sounds/piano-major7.flac -length=61.0 _MG_8817.out.2colors.tiff _MG_8817.out.2colors.tiff.wav


# enscribe retenu : sans mask, avec du hissing : -h, largeur image : 1024 -ts=1, sur une png 2 couleurs 
#enscribe -ts=0 -length=30.72 image_input.jpg sound_output.enscribe.wav

#enscribe -ts=1 -h -length=61.0 -mask /usr/local/share/horizontal/sounds/alto-majormode.flac _MG_8817.out.2colors.png _MG_8817.out.2colors.png.61s.alto-majormode.wav


# ------------- NORMALIZE ENSCRIBE AUDIO OUTPUT
# normaliser l'audio, histoire d'avoir une amplitude (volume) optimale 
#sox --norm sound_output.enscribe.wav sound_output.wav
