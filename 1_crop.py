#!/usr/bin/python
# coding: utf-8

import Image, sys
from time import gmtime, strftime

im1 = Image.open(str(sys.argv[1]))
largeur = im1.size[0]
hauteur = im1.size[1]
crop = hauteur * 3 / 4
nb = largeur/crop

# print infos image
print 
print "dimensions : %d * %d pix" % (largeur, hauteur)
print "crop : %d, nb = largeur/crop = %d" % (crop, nb)

i = 0
x1 = y1 = 0
x2 = crop
y2 = hauteur
scriptpy = str(sys.argv[1])
script = scriptpy[:-3]

while i < nb :
	
	#print infos crop
	print
	print "x1 %d" % x1
	print "y1 %d"  % y1
	print "x2 %d" % x2
	print "y2 %d" % y2
	print

	#enregistrement
	im2 = im1.crop((x1,y1,x2,y2))
	im2.save(script+".crop."+str(crop)+"."+strftime("%Y%m%d-%Hh%Mm%Ss", gmtime())+"._"+str(i)+"_.png",'PNG', quality=100)
	
	#incr boucle
	x1 = x1 + crop
	x2 = x2 + crop	
	i = i +1
