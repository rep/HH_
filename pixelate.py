from PIL import Image

'''
backgroundColor = (0,)*3
'''

pixelSize = 8

image = Image.open('pixelate_input.png')
image = image.resize((image.size[0]/pixelSize, image.size[1]/pixelSize), Image.NEAREST)
image = image.resize((image.size[0]*pixelSize, image.size[1]*pixelSize), Image.NEAREST)
pixel = image.load()

'''
for i in range(0,image.size[0],pixelSize):
  for j in range(0,image.size[1],pixelSize):
    for r in range(pixelSize):
      pixel[i+r,j] = backgroundColor
      pixel[i,j+r] = backgroundColor
'''

image.save('pixelate_output.png')


'''
load

im.load()

Allocates storage for the image and loads it from the file (or from the source, for lazy operations). In normal cases, you don’t need to call this method, since the Image class automatically loads an opened image when it is accessed for the first time.

(New in 1.1.6) In 1.1.6 and later, load returns a pixel access object that can be used to read and modify pixels. The access object behaves like a 2-dimensional array, so you can do:

pix = im.load()
print pix[x, y]
pix[x, y] = value

Access via this object is a lot faster than getpixel and putpixel.
'''

