(a) chargement_IMAGE
	(1) mise à l échelle
	(2) passage à 1 bit (noir|blanc)
	(3) si nécessaire, conversion de format en non compressé (raw)

(b) haacheuurr
	(1) plusieurs mode pour les traites de coupes :
		- tout le tour
		- juste les bords droit/gauche (plus épais si sample plus grand ?)
		- aucun contour
		- couleur blanche|noire

(c) horizontal_IMAGE
	(1) largeur Image (en pixels) / FPS = temps de diffusion

(d) horizontal_AUDIO
	(1) calcul de la FTT inverse en fonction de la hauteur de l image
