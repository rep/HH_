# ------------- FILE INFO OUTPUT
echo $1


# ------------- ROTATE IMAGE 90 DEGRES SENS HORAIRE
convert -rotate 90 $1 $1.rotate


# ------------- ENSCRIBE 
# 1536 pixels / 25 fps = 61 secondes (durée fichier audio)
# enscribe -ts=1 -hf=20 -mask /usr/local/share/horizontal/sounds/piano-major7.flac -length=61.0 _MG_8817.out.2colors.tiff _MG_8817.out.2colors.tiff.wav
#enscribe -ts=0 -length=30.72 $1.rotate $1.enscribe.wav
enscribe -ts=0 -length=82 -hf=40 $1.rotate $1.enscribe.wav


# ------------- NORMALIZE AUDIO
sox --norm $1.enscribe.wav $1.enscribe.norm.wav
echo '-- END NORM --'


# ------------- HORIZONTAL
python ./horizontal.simple.py $1


#NETTOYAGE
rm $1.rotate
rm $1.enscribe.wav
