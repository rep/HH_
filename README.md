INTENTIONS
________________________________________________________________________


Ce projet essai, en partant d'une IMAGE_SOURCE de :
- appliquer des effets à l'image (auto levels)
- la redimmensionner (en puissance de 2)
- changer son mode de couleur (en passant à du 1 bit)
- la HAACHEER (re-séquencer, de manière plus ou moins aléatoire des 
zones de l'image) pour produire une IMAGE_HAACHEE

A partir de l'IMAGE_HAACHEE :
- appliquer une ifft aux pixels et générer un FICHIER_AUDIO (enscribe)
- normaliser ce fichier audio (pour une bonne amplification)

A partir de l'IMAGE_HAACHEE :
- appliquer HORIZONTAL qui va produire une SERIE DE N IMAGES, N 
correspondant aux nombres de pixels (en largeur) de l'IMAGE SOURCE

A partir de la SERIE_DE_N_IMAGES :
- produire un FICHIER_VIDEO (de l'image ZERO à l'image N) intégrant en 
bande son le FICHIER_AUDIO



PROGRAMMES
________________________________________________________________________


0_effets.py 	: appliquer des effets à l'IMAGE_SOURCE : IMAGE_EFFET

0_hac.py 		: haacheer l'IMAGE_SOURCE pour produire IMAGE_HAACHEE

1_crop.py 		: découper IMAGE_HAACHEE en de multiples images 4/3, 
				  prêtes pour l'impression : IMAGE_PRINT

2_video2d.py 	: transformer un fichier VIDEO_SOURCE en une image 
				  reprennant et alignant une à une toutes les images de
				  VIDEO_SOURCE, pour donner VIDEO_IMAGE2D

3_enscribe_.sh	: tranformer l'IMAGE_SOURCE en un fchier audio, 
				  constituant la piste sonore de la vidéo produite par 
				  HORIZONTAL

3_horizontal.py : transforme IMAGE_HAACHEE (ou une autre au choix)
				  en un fichier video comportant autant d'images que de 
				  colonnes dans l'image




________________________________________________________________________

(a) chargement_IMAGE
	(1) mise à l échelle
	(2) passage à 1 bit (noir|blanc)
	(3) si nécessaire, conversion de format en non compressé (raw)

(b) haacheuurr
	(1) plusieurs mode pour les traites de coupes :
		- tout le tour
		- juste les bords droit/gauche (plus épais si sample plus grand 
		?)
		- aucun contour
		- couleur blanche|noire

(c) horizontal_IMAGE
	(1) largeur Image (en pixels) / FPS = temps de diffusion

(d) horizontal_AUDIO
	(1) calcul de la FTT inverse en fonction de la hauteur de l image




TODO
________________________________________________________________________


_IMAGE SOURCE_
 + EFFECTS (auto levels)
 + RESIZE (2^n)
 + COLOR MODE (1 bit)
 + HAACHEUUR
 	|
	|_IMAGE HAACHEE
	| + ENSCRIBE
	| + NORMALIZE (sox)
	|	|_FICHIER AUDIO
	|
	| + CROP
	|
	| + HORIZONTAL
		|
		|_FICHIERS IMAGES
		  + FFMPEG
			|_FICHIER VIDEO



FFMPEG ONE LINER
________________________________________________________________________

#input AVI, output WEBM
ffmpeg -i hachures-out-2011-01-30-12h05m57s-0.jpg.resized.jpg.avi -c:v libvpx -crf 10 -b:v 1M -c:a libvorbis output.webm

#input WEBM, output WEBM + audio
ffmpeg -i output.webm -i hachures-out-2011-01-30-12h05m57s-0.jpg.wav -acodec libvorbis -shortest output_audio.webm

#input AVI, output webm vpx (CRF) + audio inpujt WAV, audio output libvorbis
ffmpeg -i hachures-out-2011-01-30-12h05m57s-0.jpg.resized.jpg.avi -i hachures-out-2011-01-30-12h05m57s-0.jpg.wav -c:v libvpx -crf 10 -b:v 2M -c:a libvorbis -threads 4 output_audio.webm

#input : suite d'images + audio (wav) output x264 mp4, audio libfaac
ffmpeg -i horizontal%d.png -i pylone3.png.enscribe.norm.wav  -c:a libfaac -c:v libx264 -preset ultrafast -crf 32 final.mp4
ffmpeg -i horizontal%d.png -i pylone3.png.enscribe.norm.wav  -c:a libfaac -c:v libx264 -b:v 5000k  final2.mp4
# pareil mais en essayant un format de pixel en niveau de gris, ou meme 1 bit
ffmpeg -i horizontal%d.png -i pylone3.png.enscribe.norm.wav  -c:a libfaac -c:v libx264 -vf format=gray -b:v 5000k  final2.mp4
ffmpeg -i horizontal%d.png -i pylone3.png.enscribe.norm.wav  -c:a libfaac -c:v libx264 -pix_fmt monow  final3.mp4




