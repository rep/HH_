#!/usr/bin/python
# coding: utf-8

# haacheuur 0.24
# port industriel de port la nouvelle - couleur - 60cm*30cm
# image source : pln.jpg
# image obtenue : pln..20150910-11h59m53s.jpg

# pln.png 3936x1024 pix

'''
image input puredata : 16384 width x 768 height
donc par exemple, pour 3 images :
3 x 16384 = 49152 pixels en tout
'''

import sys
import Image
import random
import os
import ImageDraw
import ImageFont
import ImageFilter
from time import gmtime, strftime
import time

if not len(sys.argv) > 1:
    raise SystemExit("Usage: %s image_source" % sys.argv[0])

# modifs du 30/10/2013
import ImageEnhance


#rapport d'allongement de la nouvelle image par rapport à la largeur de l'image originale
allongement = 1


#ouverture de l'image source et conversion en mode couleur 1bit
#1 bit (sys.argv[1])).convert('1') et niveaux de gris : (sys.argv[1])).convert('L')
im1 = Image.open(str(sys.argv[1]))
im2 = im1.copy()
im3 = Image.new("RGBA",(im1.size[0], im1.size[1]))
im5 = Image.new("RGBA",(im1.size[0], im1.size[1]))
im4 = Image.new("RGBA",(im1.size[0]*allongement, im1.size[1]))
#im4 = Image.new("RGBA",(49152, im1.size[1]))

Larg = im1.size[0]
Haut = im1.size[1]
import pickle

loadfile = False

class Sequence:
	def __init__(s):
		randomCoupeHauteur = []
		s.randomCopyPosi =[]
		s.proportions=[]
		s.choix=[]
		s.sizeOutput=None
		s.sizeInput=None
	
"""
seq = dict()
seq["randomCoupeHauteur"] = []
seq["randomCopyPosi"] = []
seq["proportions"] = []
seq["choix"] = []
seq["sizeOutput"]= im4.size
seq["sizeInput"] = im1.size
"""

if loadfile:
	seq=pickle.load(open("test.pickle"))

else :
	seq=Sequence()
	
for i in range(1):
	# constitution de la liste des tranches horizontales
	# genre comme si qu'on avait un 16 pistes :)
	# nombre aleatoire compris dans les limites de l'image
	def randHaut():
		return random.randint(0, im1.size[1]/8)*8

	if loadfile:
		randomCoupeHauteur = seq.randomCoupeHauteur

	else:
		randomCoupeHauteur = [0, \
		randHaut(),randHaut(),randHaut(),randHaut(), \
		randHaut(),randHaut(),randHaut(),randHaut(), \
		randHaut(),randHaut(),randHaut(),randHaut(), \
		randHaut(),randHaut(),randHaut(),randHaut(), \
		randHaut(),randHaut(),randHaut(),randHaut(), \
		randHaut(),randHaut(),randHaut(),randHaut(), \
		randHaut(),randHaut(),randHaut(),randHaut(), \
		randHaut(),randHaut(),randHaut(),randHaut(), \
		im1.size[1]]
		# rangement des valeurs des plus petites au plus grandes
		randomCoupeHauteur.sort()
		seq.randomCoupeHauteur = randomCoupeHauteur

	# les hachures
	def Hacheur(haut, bas) :

		n=0
		i=0
		#!!!!!!!!!!
		while n<im4.size[0] :
			if loadfile:
				proportions = seq.proportions
				choix_rnd = seq.choix[i]
				randomCopyPosi = seq.randomCopyPosi[i]
			else:

				'''
				8 16 24 32 40 48 56 64 72 80 88 96 104 112 120 128 136 144 152 160 168
				'''

				#proportions = [(160,6),(120,4),(120,8),(80,16),(60,16),(20,32),(8,24),(8,16),(5,64),(3,24),(2,24),(1,256),(1,128),(1,64),(1,32),(1,16),(1,8)]
				#proportions = [(160,6),(120,4),(120,8),(80,16),(60,16),(20,32),(8,24),(8,16),(5,64),(3,24),(2,24),(1,256),(1,128),(1,64),(1,32),(1,16),(1,8)]
				#proportions = [(1280,1),(800,1),(800,2),(640,1),(640,2),(640,4),(320,4),(320,2),(320,1),(160,1),(120,2),(120,3),(80,6),(64,12),(64,6),(32,4),(32,8),(32,16),(32,32),(32,64)]

				proportions = [(16,2),(8,2),(4,2),(2,2),(1,2)]

				#proportion pln pour output final avec puredata/gem (decalage phase 32pix)
				
				'''
				proportions = [	(768,2),\
								(512,4),\
								(256,8),\
								(128,4),(128,16),\
								(64,4),(64,8),(64,16),\
								(32,8),(32,16),(32,32),(32,32),(32,64),(32,128),\
								(16,1),(16,4),(16,16),(16,64)]
				'''
				#proportions = [(512,1),(256,2),(128,4),(64,8),(64,4),(32,16),(32,32),(32,8),(16,8),(16,32),(8,64),(8,32),(8,16)]
				#proportions = [(32,1),(32,2),(32,3),(32,4),(32,5),(32,6),(32,8),(32,12),(32,16),(32,24),(32,32),(32,64)]
				
				seq.proportions = proportions
				#proportions = seq.proportions[]
				choix_rnd = random.randint(0, len(proportions)-1)
				#choix_rnd = random.randint(0, len(proportions)-1)
				seq.choix.append(choix_rnd)
				
				largeur = proportions[choix_rnd][0]
				
				# positionnement de la copie, aleatoirement, entre 0 et la largeur totale de l'image 
				randomCopyPosi = random.randint(0, (im1.size[0]-largeur))
				#randomCopyPosi = seq.randomCopyPosi
				seq.randomCopyPosi.append(randomCopyPosi)

			i+=1
			
			# tirage au sort
			#seq["choix"].append(choix_rnd)

			# assignation des valeurs (paires) finales choisies
			largeur = proportions[choix_rnd][0]			
			repeat = proportions[choix_rnd][1]

			#seq["randomCopyPosi"].append(randomCopyPosi)

			# pixellisation : choix de la taille du pixel = pixelSize
			#pixelSizeList = [1,1,1,1,1,1,1,1,4,8]
			pixelSizeList = [1,2,4,8]
			pixelSizeIndex = random.randint(0,len(pixelSizeList)-1)
			pixelSize = pixelSizeList[pixelSizeIndex]
			
			cropfinal = [largeur,bas-haut]
			hauteur = bas-haut
			'''
			haut gauche(0,0).
			haut droit (largeur-1 , 0).
			bas gauche (0 , hauteur-1).
			bas droit (largeur-1, hauteur-1)
			'''
			
			if largeur % pixelSize != 0:
				croop = largeur / pixelSize
				largeur = (int(croop) + 1 ) * pixelSize
				
			if  hauteur % pixelSize != 0:
				croop2 = hauteur / pixelSize
				hauteur = (int(croop2) + 1 ) * pixelSize
				
			# decoupage du sample
			im5 = im2.crop((randomCopyPosi,haut,randomCopyPosi+largeur,haut+hauteur))
			
			'''COLLAGE DU SAMPLE '''
			loop = 0
			while loop<repeat:	
				px1 = n
				px2 = n + largeur
#				draw = ImageDraw.Draw(im3)
				
				''' PIXELISATION'''
				im5 = im5.resize((im3.size[0]/pixelSize, im3.size[1]/pixelSize), Image.NEAREST)
				im5 = im5.resize((im3.size[0]*pixelSize, im3.size[1]*pixelSize), Image.NEAREST)
				im3 = im5.crop(0,0,cropfinal[0]-1,cropfinal[1]-1)
				
				#lignes colorées de 1 pix
				draw.line((im3.size[0]-1, 0, im3.size[0]-1, im3.size[1]-1), fill="rgb(255,255,255)")
				im4.paste(im3, (px1, haut, px2, bas))
				
				n = n + largeur
				loop = loop + 1
	
	''' OPTIONS'''	
	def TrancheHorizontale() :
		# tirage au hasard de la bande copiee
		pos = random.randint(0, im1.size[1]-im1.size[1]/20)
		# copiage
		im5 = im2.crop((0,pos,im1.size[0],pos+im1.size[1]/20))
		
		# le soulignage en blanc
		draw = ImageDraw.Draw(im5)
		draw.line((0, im5.size[1]-1, im5.size[0], im5.size[1]-1), fill="black")
		draw.line((0, 1, im5.size[0], 1), fill="black")
		
		# collage	
		im4.paste(im5, (0,pos,im1.size[0],pos+im1.size[1]/20))

	# HAACHEUUR
	for j in range(len(randomCoupeHauteur)-1):
		Hacheur(randomCoupeHauteur[j], randomCoupeHauteur[j+1])
	
	''' SAUVEGARDE	
	'''
	
	# CTRL + S
	#chemin du script	: scriptpy = sys.argv[0]
	#chemin de l'image	: str(sys.argv[1])
	scriptpy = str(sys.argv[1])
	script = scriptpy[:-3]
	n = "%05d" % i
	im4.save(script+"."+n+"_"+strftime("%Y%m%d-%Hh%Mm%Ss", gmtime())+".png",'PNG', quality=100)
	
	#pickle.dump(seq, open("test.pickle","w"))
	pickle.dump(seq, open(script+"."+n+"_"+strftime("%Y%m%d-%Hh%Mm%Ss", gmtime())+".pickle","w"))
	#print script+"."+str(i)+"_"+strftime("%Y%m%d-%Hh%Mm%Ss", gmtime())+".jpg"
	#millis = int(round(time.time() * 1000))
	#print "millis-secondes : "+str(millis)



#obtenir les valeurs RGB des pixels de l'image
'''
from PIL import Image

im = Image.open('test.png')
pixels = list(im.getdata())
width, height = im.size

pixels = [pixels[i * width:(i + 1) * width] for i in xrange(height)]

for r in range(0, height-⁠1) :
        print "pixels = "+str(pixels[r])

print " "
print "len(pixels) = "+str(len(pixels))
print "width = "+str(width)
print "height = "+str(height)
print " "

#tintin = im.tostring( "raw", "RGBA", 0, -⁠1)
#print "tostring = "+tintin
'''
