#!/usr/bin/python
# -*- coding: utf-8 -*- 
import sys
import Image
import random
import os
import ImageDraw
import ImageFont
import ImageFilter
from time import gmtime, strftime
# modifs du 30/10/2013
import ImageEnhance

im1 = Image.open(str(sys.argv[1]))
im1 = im1.convert('RGB')

# BLUR
'''for i in range(40):
    im1 = im1.filter(ImageFilter.BLUR)
'''

#operation 'degoulinage', pixel par pixel
imagesource = im1
imagedest = im1
for i in range (60000) :
	posrandX = random.randint(1, imagesource.size[0]-1)
	posrandY = random.randint(1, imagesource.size[1]-55)
	pixelRGB = imagesource.getpixel((posrandX,posrandY))
	
	for Y in range (1, 49) :
		imagedest.putpixel((posrandX, posrandY + random.randint(1,Y)+5), pixelRGB)
		
imagedest.save("test.jpg"+strftime("%Y-%m-%d-%Hh%Mm%Ss", gmtime())+".jpg",quality=100)

'''
#collage de l'image résultante dans une image 1bit
im4 = Image.new("1", (im1.size[0], im1.size[1]))
im4.paste(im1, (0, 0, im1.size[0], im1.size[1]))

# CTRL + S
im4.save("griis-out-"+strftime("%Y-%m-%d-%Hh%Mm%Ss", gmtime())+".jpg",quality=100)
'''

#COPIER/COLLAGE SIMPLE ET EFFICACE :
'''
box = (30, 30, 110, 110)
sample = image.crop(box)
for i in range(10):  # with the BLUR filter, you can blur a few times to get the effect you're seeking
    sample = sample.filter(ImageFilter.BLUR)
image.paste(sample, box)
'''
